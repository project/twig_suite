<?php

namespace Drupal\twig_suite\TwigExtension;

/**
 * Trait NodeTrait.
 *
 * @package Drupal\twig_suite\TwigExtension
 */
trait NodeTrait {

  /**
   * Get current node by parameter.
   *
   * @param string $param
   *   The param name.
   *
   * @return mixed|null
   *   Current node or null.
   */
  public function drupalCurrentNode($param = 'node') {
    return \Drupal::routeMatch()->getParameter($param);
  }

}
