<?php


namespace Drupal\twig_suite\TwigExtension;


use Drupal\field\Entity\FieldConfig;

trait FieldTrait {

  /**
   * Loads a field config entity based on the entity type and field name.
   *
   * @param string $entity_type_id
   *   ID of the entity type.
   * @param string $bundle
   *   Bundle name.
   * @param string $field_name
   *   Name of the field.
   *
   * @return \Drupal\field\Entity\FieldConfig The field config entity if one exists for the provided field
   *   The field config entity if one exists for the provided field
   *   name, otherwise NULL.
   */
  public function drupalFieldConfig($entity_type_id, $bundle, $field_name) {
    $config = FieldConfig::loadByName($entity_type_id, $bundle, $field_name);
    return $config;
  }

}
